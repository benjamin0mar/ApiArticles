const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const app = express();


var router = express.Router();


router.use(function(req, res, next) {
  // do logging
  console.log('Something is happening.');
  next(); // make sure we go to the next routes and don't stop here

});

mongoose.connect("mongodb://localhost/articles");

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var articleScheme = {
  title: String,
  content: String,
  link: String
}

var Article = mongoose.model("Article", articleScheme);

app.get('/', function(req, res) {
  res.json({ message: 'hooray! welcome to our api!' });   
});

router.get('/', function(req, res) {
  res.json({ message: 'hooray! welcome to our api!' });   
});

router.route('/articles')

  .post(function(req, res) {

      var article = new Article(); 

      console.log(req.body)

      article.title = req.body.title; 
      article.content = req.body.content;  
      article.link = req.body.link;  

      article.save(function(err) {
          if (err)
              res.send(err);

          res.json({ message: 'article created!' });
      });
  })


  .get(function(req, res) {
    
    Article.find(function(err, articles){
      if (err)
        res.send(err);
      res.json(articles)
    });

  });


router.route('/articles/:id')

    .put(function(req, res) {
      Article.findById(req.params.id, function(err, article) {
        if (err)
            res.send(err);

        article.title = req.body.title; 
        article.content = req.body.content;  
        article.link = req.body.link;  

        article.save(function(err) {
          if (err)
              res.send(err);

          res.json({ message: 'article updated!' });
        });

      });
    })


    .delete(function(req, res) {

      Article.remove({
          _id: req.params.id
      }, function(err, bear) {
          if (err)
              res.send(err);
          res.json({ message: 'Successfully deleted' });
      });
    });  


app.use('/api', router);



app.listen("8080", function(){
  console.log("Server is booted...")
});